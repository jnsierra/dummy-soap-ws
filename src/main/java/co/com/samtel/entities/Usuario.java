package co.com.samtel.entities;

public class Usuario {
	
	private String nombre;
	private String usuario;
	private Long id;
	
	
	
	public Usuario() {
		super();
	}
	public Usuario(String nombre, String usuario, Long id) {
		super();
		this.nombre = nombre;
		this.usuario = usuario;
		this.id = id;
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	

}
