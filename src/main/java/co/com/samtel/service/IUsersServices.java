package co.com.samtel.service;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebResult;
import javax.jws.WebService;

import co.com.samtel.entities.Usuario;

@WebService(name =  "UsersService")
public interface IUsersServices {
	
	@WebMethod
	@WebResult(name =  "ListUsers")
	public List<Usuario> getUsers();

}
