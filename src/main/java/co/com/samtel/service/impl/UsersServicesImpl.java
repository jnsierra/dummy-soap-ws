package co.com.samtel.service.impl;

import java.util.ArrayList;
import java.util.List;

import co.com.samtel.entities.Usuario;
import co.com.samtel.service.IUsersServices;

public class UsersServicesImpl implements IUsersServices {

	@Override
	public List<Usuario> getUsers() {
		List<Usuario> usuarios = new ArrayList<>();
		usuarios.add(new Usuario("Jhon", "JhonDuvanS", Long.valueOf(1)));
		usuarios.add(new Usuario("Nicolas", "Moga", Long.valueOf(2)));
		return usuarios;
	}

}
