package co.com.samtel.config;

import javax.xml.ws.Endpoint;

import org.apache.cxf.Bus;
import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import co.com.samtel.service.impl.UsersServicesImpl;

@Configuration
public class CXFConfig {

	@Bean
	public ServletRegistrationBean dispacherServlet() {
		return new ServletRegistrationBean(new CXFServlet(), "/services/*");
	}
	
	@Bean(name = Bus.DEFAULT_BUS_ID)
	public SpringBus springBus() {
		SpringBus springBus = new SpringBus();
		return springBus;
	}
	
	@Bean
	public Endpoint endpoint() {
		EndpointImpl endpointImpl = new EndpointImpl(springBus(), new UsersServicesImpl());
		endpointImpl.publish("/users");
		return endpointImpl;
	}
	
}
